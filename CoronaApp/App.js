import React from "react";
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Login from './pages/Login'
import Home from './pages/Home'
import Detail from './pages/Detail'
import About from './pages/About'
import { Provider } from "react-redux";
import store from "./redux/store/index";
import { MaterialCommunityIcons } from '@expo/vector-icons';

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const TabsScreen = () => (

  <Tabs.Navigator tabBarOptions={{
    activeTintColor: '#9F0000'
  }} >
    <Tabs.Screen name="Home" component={Home} options={{
      tabBarLabel: 'Home',
      tabBarIcon: ({ color, size }) => (
        <MaterialCommunityIcons name="home" color={color} size={size} />
      )
    }} />
    <Tabs.Screen name="Detail" component={Detail} options={{
      tabBarLabel: 'Detail',
      tabBarIcon: ({ color, size }) => (
        <MaterialCommunityIcons name="view-list" color={color} size={size} />
      )
    }} />
    <Tabs.Screen name="About" component={About} options={{
      tabBarIcon: ({ color, size }) => (
        <MaterialCommunityIcons name="account-circle" color={color} size={size} />
      )
    }} />
  </Tabs.Navigator>
)

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
          <Stack.Screen name="TabsScreen" component={TabsScreen} options={{ headerShown: false }} />

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
