import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function ProvinsiCard(props) {

    function thousandFormat(num) {
        return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    return (
        <View style={styles.boxProvinsi}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                <Text style={styles.textNumber}>{props.idx + 1}.</Text>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.textNumber}>{props.provinsi.Provinsi}</Text>
                    <Text style={{ fontSize: 18, color: 'red' }}>{thousandFormat(props.provinsi.Kasus_Posi)} Pasien Positif</Text>
                </View>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    boxProvinsi: {
        display: 'flex',
        backgroundColor: "white",
        height: 120,
        width: 320,
        borderRadius: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5,
        marginVertical: 5,
        justifyContent: 'center'
    },
    textNumber: {
        fontSize: 22,
        color: '#9F0000'
    }
})