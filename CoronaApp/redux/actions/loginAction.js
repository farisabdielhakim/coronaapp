export const setLogin = (data) => {
    return (dispatch) => {
        dispatch({
            type: "SETLOGIN",
            payload: { isLogin: true, userName: data.userName }
        })
    }
}