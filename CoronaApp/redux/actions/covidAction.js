import axios from 'axios';

export const getProvinsiData = () => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `https://api.kawalcorona.com/indonesia/provinsi`
        })
            .then(({ data }) => {
                let newData = []
                data.forEach(element => {
                    newData.push(element.attributes)
                });
                let newDataSort = newData.sort(function (a, b) {
                    return b.Kasus_Posi - a.Kasus_Posi;
                });
                dispatch({
                    type: "getProvinsiData",
                    payload: newDataSort
                })
            })
    }
}

export const getIndonesiaData = () => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `https://api.kawalcorona.com/indonesia`
        })
            .then(({ data }) => {
                dispatch({
                    type: "getIndonesiaData",
                    payload: data[0]
                })
            })
    }
}

