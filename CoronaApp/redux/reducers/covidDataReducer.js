const initialState = {
    provinsiData: [],
    indoData: []
}

function covidDataReducer(state = initialState, action) {
    if (action.type === "getProvinsiData") {
        return { ...state, provinsiData: action.payload };
    }
    if (action.type === "getIndonesiaData") {
        return { ...state, indoData: action.payload };
    }
    return state;
}

export default covidDataReducer;