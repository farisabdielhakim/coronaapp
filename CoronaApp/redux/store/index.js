import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import userLoginReducer from "../reducers/userLoginReducer"
import covidDataReducer from "../reducers/covidDataReducer";

const reducers = combineReducers({ userLoginReducer, covidDataReducer })

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
