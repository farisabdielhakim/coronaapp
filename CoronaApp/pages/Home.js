import React, { useEffect } from 'react';
import { StyleSheet, Image, Text, View, FlatList, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { getIndonesiaData } from '../redux/actions/covidAction'

export default function Home() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getIndonesiaData())
    }, [])

    const { userName } = useSelector(state => state.userLoginReducer)
    const { indoData } = useSelector(state => state.covidDataReducer)
    return (
        < View style={styles.container}>
            <View style={styles.header}>
                <Text style={{ ...styles.text, ...styles.whiteText }}>Hi, {userName}</Text>
                <Image source={require('../assets/homeImage.png')} style={{ width: 322, height: 158 }}></Image>
                <View style={styles.headerDesc}>
                    <Text style={{ ...styles.text, ...styles.whiteText }}>Stay At Home, Stay Safe, {userName}</Text>
                </View>
            </View>
            <View style={styles.title}>
                <Text style={{ color: '#9F0000', fontSize: 28 }}>COVID-19 di Indonesia</Text>
                <Text>See Detail</Text>
            </View>
            <View>

                <View style={styles.content}>
                    <View style={styles.boxContent}>
                        <Text style={{ fontSize: 30, color: 'black' }}>{indoData.positif}</Text>
                        <Text style={{ fontSize: 17, color: 'black' }}>Pasien Positif</Text>
                    </View>
                    <View style={styles.boxContent}>
                        <Text style={{ fontSize: 30, color: 'red' }}>{indoData.meninggal}</Text>
                        <Text style={{ fontSize: 17, color: 'red' }}>Pasien Meninggal</Text>
                    </View>
                </View>
            </View>
            <View style={styles.content}>
                <View style={styles.boxContent}>
                    <Text style={{ fontSize: 30, color: 'orange' }}>{indoData.dirawat}</Text>
                    <Text style={{ fontSize: 17, color: 'orange' }}>Pasien Dirawat</Text>
                </View>
                <View style={styles.boxContent}>
                    <Text style={{ fontSize: 30, color: 'green' }}>{indoData.sembuh}</Text>
                    <Text style={{ fontSize: 17, color: 'green' }}>Pasien Sembuh</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        display: 'flex'
    },
    header: {
        flexDirection: 'row',
        paddingLeft: 30,
        marginBottom: 10,
        marginTop: 25,
        paddingTop: 10,
        backgroundColor: '#9F0000',
        width: 400,
        height: 230,
        borderBottomLeftRadius: 55,
        borderBottomRightRadius: 55,
    },
    title: {
        marginBottom: 30,
        marginTop: 30
    },
    content: {
        flexDirection: 'row',
        paddingLeft: 23
    },
    boxContent: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5,
        padding: 5,
        width: 150,
        marginRight: 30
    },
    text: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    whiteText: {
        color: '#fff'
    },
    headerDesc: {
        flex: 2
    }
})
