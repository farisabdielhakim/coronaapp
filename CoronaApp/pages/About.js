import React from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { StyleSheet, Text, View, Image } from 'react-native';
export default function AboutScreen() {

    return (
        <View style={styles.container}>
            <Text style={{ marginTop: 15, fontSize: 30, color: "#fff", marginTop: 0, fontWeight: "bold" }}>Tentang Saya</Text>
            <Image source={require('../assets/profile.jpg')} style={{
                width: 100,
                height: 100,
                borderRadius: 300,
                marginTop: 10,
                borderColor: "#fff",
                borderWidth: 3
            }} />
            <Text style={{ marginTop: 15, fontSize: 25, color: "#fff", fontWeight: 'bold' }}>Faris Abdi El Hakim</Text>
            <Text style={{ marginTop: 5, fontSize: 15, color: "#fff" }}>React Native Developer</Text>
            <View style={styles.boxKontak}>
                <Text style={{ borderBottomWidth: 1, marginBottom: 5, textAlign: 'center' }}>Hubungi Saya</Text>
                <View style={styles.listKontak}>
                    <View style={styles.detailKontak}>
                        <FontAwesome name="facebook-square" size={30} color="#9F0000" />
                        <Text style={{ marginLeft: 10 }}>Faris Abdi El Hakim</Text>
                    </View>
                    <View style={styles.detailKontak}>
                        <FontAwesome name="instagram" size={30} color="#9F0000" />
                        <Text style={{ marginLeft: 10 }}>@farisabdiel16</Text>
                    </View>
                    <View style={styles.detailKontak}>
                        <FontAwesome name="twitter" size={30} color="#9F0000" />
                        <Text style={{ marginLeft: 10 }}>@_FEL_16</Text>
                    </View>
                </View>
            </View>
            <View style={styles.boxPorto}>
                <Text style={{ borderBottomWidth: 1, paddingLeft: 5, textAlign: 'center' }}>Portofolio</Text>
                <View style={styles.listPorto}>
                    <View style={styles.detailPorto}>
                        <FontAwesome name="gitlab" size={30} color="#9F0000" />
                        <Text style={{ marginLeft: 10 }}>Farisabdielhakim</Text>
                    </View>
                    <View style={styles.detailPorto}>
                        <FontAwesome name="github" size={30} color="#9F0000" />
                        <Text style={{ marginLeft: 10 }}>Farisabdielhakim</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#9F0000',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    boxPorto: {
        display: 'flex',
        backgroundColor: "white",
        height: 100,
        width: 320,
        marginBottom: 10,
        borderRadius: 10,
        marginTop: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5
    },
    boxKontak: {
        display: 'flex',
        backgroundColor: "white",
        height: 150,
        width: 320,
        borderRadius: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 10,
        marginTop: 10
    },
    listKontak: {
        alignItems: 'center'
    },
    detailKontak: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 5
    },
    detailPorto: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 10
    },
    listPorto: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});
