import React, { useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { getProvinsiData } from '../redux/actions/covidAction'
import ProvinsiCard from '../components/ProvinsiCard'

export default function Detail() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getProvinsiData())
    }, [])

    const { provinsiData } = useSelector(state => state.covidDataReducer)

    return (
        < View style={styles.container}>
            <View style={styles.appMenu}>
                <Text style={styles.text}>Provinsi Yang Terkena COVID-19</Text>
                <FlatList
                    data={provinsiData}
                    renderItem={({ item, index }) => <ProvinsiCard provinsi={item} idx={index} />}
                    keyExtractor={(key, index) => index.toString()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#9F0000',
        alignItems: 'center',
        display: 'flex'
    },
    appMenu: {
        marginTop: 20,
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold'
    }
})
