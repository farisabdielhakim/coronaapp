import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux'
import { setLogin } from '../redux/actions/loginAction'

export default function LoginScreen(props) {
    const dispatch = useDispatch();
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const [isError, setIsError] = useState(false)

    const handleLogin = () => {
        if (password === '12345') {
            dispatch(setLogin({ userName: userName }))
            props.navigation.push('TabsScreen')
        } else {
            setIsError(true)
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.appName}>
                <Image source={require('../assets/coronaIcon.png')} style={{ width: 137, height: 137, marginTop: 40 }} />
                <Text style={styles.text}>COVID-19</Text>
            </View>
            <View style={styles.body}>
                <Image source={require('../assets/loginImg.png')} style={styles.imgLogin}></Image>
                <TextInput onChangeText={userName => setUserName(userName)} placeholder="USERNAME" placeholderTextColor='grey' style={styles.textInput}></TextInput>
            </View>
            <View style={styles.inputText}>
                <TextInput onChangeText={password => setPassword(password)} secureTextEntry={true} placeholderTextColor='grey' placeholder="PASSWORD" style={styles.textInput}></TextInput>
            </View>
            <View>
                <Text style={{ color: 'white', fontStyle: 'italic' }}>(password:12345)</Text>
                <Text style={isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
                <TouchableOpacity style={styles.buttonSignIn} onPress={() => handleLogin()}>
                    <Text style={styles.textButton}>Sign In</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#9F0000',
        alignItems: 'center',
        display: 'flex'
    },
    appName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        fontSize: 30,
        color: 'white'
    },
    body: {
        marginTop: 30,
        paddingBottom: 10,
        alignItems: 'center'
    },
    textInput: {
        backgroundColor: "rgba(52, 52, 52, 0.5)",
        width: 320,
        height: 64,
        borderColor: "white",
        borderWidth: 4,
        borderRadius: 20,
        paddingLeft: 10,
        textAlign: 'center',
        fontSize: 24,
        color: 'white'
    },
    buttonSignIn: {
        height: 30,
        alignItems: 'center'
    },
    textButton: {
        backgroundColor: "rgba(52, 52, 52, 0.5)",
        opacity: 50,
        borderColor: "white",
        borderWidth: 4,
        color: "white",
        paddingHorizontal: 20,
        paddingVertical: 5,
        borderRadius: 20,
        fontSize: 30,
        fontWeight: 'bold'
    },
    errorText: {
        color: 'white',
        textAlign: 'center',
        marginBottom: 1,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 1,
    },
    imgLogin: {
        width: 322,
        height: 322,
        position: 'absolute'
    }
});